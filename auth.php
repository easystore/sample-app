<?php

// 
// This file serve for redirect url.
// EasyStore redirect user to this URL once they completed oauth process.
//

include '_inc.php';
include 'EasyStoreSDK.php';

$query = $_GET;
$query_hmac = $query["hmac"];
unset($query["hmac"]);

$sdk = new EasyStore(CLIENT_ID, CLIENT_SECRET, $_GET["shop"]);

$generated_hmac = $sdk->generate_hmac( urldecode(http_build_query($query)) );

if($sdk->verify_hmac($query_hmac, $generated_hmac)){

    // Exchange access token with EasyStore by passing over $_GET["code"]
    //
    // ***IMPORTANT***
    // save access_token into your database, you will need it for further actions. 
    // For example, retrieve EasyStore sellers store information
    //
    $access_token = $sdk->get_access_token($_GET["code"]);

    $sdk->set_access_token($access_token);

    $store = $sdk->get_store_detail();
    // var_dump($store);

    //
    // Subscribe uninstall your app event webhook
    // Please refer our storefront API documentation for webhook topics list
    // https://documenter.getpostman.com/view/1586449/RWTiwKms?version=latest#889b7031-549f-167b-fb1c-ac5a48f3f45f
    //

    $webhook = $sdk->subscribe_webhook([
        "url" => YOUR_APP_HOST."/webhook-uninstall-app.php",
        "topic" => "app/uninstall"
    ]);

    //
    // Register response shipping rates endpoint
    // So that buyers able to choose one of your services as shipping courier during checkout process
    //

    $curl = $sdk->register_curl([
        "url" => YOUR_APP_HOST."/storefront/get-shipping-rate.php",
        "topic" => "shipping/list/non_cod"
    ]);

    // redirect user to setting page or any destination
    header("Location: setting.php");

}



