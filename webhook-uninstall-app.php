<?php

//
// EasyStore will fire uninstall app event webhook to this URL.
//

include '_inc.php';
include 'EasyStoreSDK.php';

$shop = $_SERVER["HTTP_EASYSTORE_SHOP_DOMAIN"];
$data = file_get_contents('php://input');

$sdk = new EasyStore(CLIENT_ID, CLIENT_SECRET, $shop);

if($_SERVER["HTTP_EASYSTORE_TOPIC"] == 'app/uninstall'){

    $generated_hmac = $sdk->generate_hmac($data);

    if($sdk->verify_hmac($_SERVER["HTTP_EASYSTORE_HMAC_SHA256"], $generated_hmac)){


        //
        // Perform delete record action
        // 


    }else{

        echo json_encode(["errors" => "Hmac validate fail"]);

    }

}else{

    echo json_encode(["errors" => "Topic invalid"]);

}