<?php

// 
// This file serve as shipping rate provider.
// EasyStore will request from this URL to get shipping rate from your end.
//

include __DIR__.'/../_inc.php';
include __DIR__.'/../EasyStoreSDK.php';


$shop = $_SERVER["HTTP_EASYSTORE_SHOP_DOMAIN"];
$data = file_get_contents('php://input');

$sdk = new EasyStore(CLIENT_ID, CLIENT_SECRET, $shop);

if($_SERVER["HTTP_EASYSTORE_TOPIC"] == 'shipping/list/non_cod'){

    $generated_hmac = $sdk->generate_hmac($data);

    if($sdk->verify_hmac($_SERVER["HTTP_EASYSTORE_HMAC_SHA256"], $generated_hmac)){

        // $data["currency_code"]
        // $data["shipping_state"]
        // $data["shipping_city"]
        // $data["shipping_zip"]
        // $data["shipping_country"]
        // $data["total_item_weight"]
        // $data["total_item_quantity"]
        // $data["subtotal_price"]
        // $data["total_discount"]  
        //Return courier services list based on request parameters given.

        echo json_encode([

            "rate" => [

                [            
                    "id" => "ep0001",
                    "name" => "Skynet",
                    "remark" => "",
                    "handling_fee" => 10,
                    "shipping_charge" => 6.00,
                    "courier_name" => "Skynet",
                    "courier_url" => "https://s3-ap-southeast-1.amazonaws.com/easyparcel-static/Public/img/couriers/Skynet.jpg",
                ],
                [            
                    "id" => "ep0002",
                    "name" => "PosLaju",
                    "remark" => "",
                    "handling_fee" => 6.50,
                    "shipping_charge" => 0.00,
                    "courier_name" => "PosLaju",
                    "courier_url" => "https://s3-ap-southeast-1.amazonaws.com/easyparcel-static/Public/img/couriers/Pos_Laju.jpg",
                ]                
    

            ]

        ]);


    }else{

        echo json_encode(["errors" => "Hmac validate fail"]);

    }

}else{

    echo json_encode(["errors" => "Topic invalid"]);

}