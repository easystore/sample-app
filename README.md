# Sample app (Logistic)

A simple demonstrate on how to develop logistic related EasyStore app.

### Steps on how to configure Sample app on your server

1. Log in to [EasyStore partner portal](https://partners.easystore.co).
   - email: demo@easystore.co
   - password: easystore#demo
2. Visit [Sample app setting](https://partners.easystore.co/apps/218)
3. Replace `App URL` with your app host. For example, `https://yourapphost.com/index.php`
4. Replace `Fulfillment URL` with your app host. For example, `https://yourapphost.com/admin/fulfillment-url.php`
5. Replace `Redirection URL` with your app host. For example, `https://yourapphost.com/admin/auth.php`
6. Download all files from this repository and host in your server.
7. Replace `CLIENT_ID` with your app CLIENT_ID in `_inc.php`.
8. Replace `CLIENT_SECRET` with you app CLIENT_SECRET `_inc.php`.
9. Replace `YOUR_APP_HOST` with your development host in `_inc.php`.
10. Replace `REDIRECT_URL` with your own redirect URL in `_inc.php`.
11. Install [Sample app](https://www.easystore.co/en-my/apps/sample-app) into your development / trial store.

### References

- [EasyStore developer site](https://developers.easystore.co)
- [Public API documentation](https://documenter.getpostman.com/view/1586449/RWTiwKms?version=latest)
