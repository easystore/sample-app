<?php

// 
// This file serve for fulfil order UI.
// EasyStore redirect sellers to this URL, if they choose to fulfil their order with your app.
//

include __DIR__.'/../_inc.php';
include __DIR__.'/../EasyStoreSDK.php';

$shop = str_replace("https://", NULL, $_GET["shop"]);
$order_id = $_GET["order_id"];
$order_token = $_GET["token"];

$sdk = new EasyStore(CLIENT_ID, CLIENT_SECRET, $shop);

//
// **IMPORTANT**
// In order to able retrieve order from EasyStore API successfully
// set access_token that belong to the $shop in request header.
// For example, select access_token from apps.easystore where shop = '$shop';
//
$access_token_in_db = "b66c6e4700057e18073c5891de69921b";
$sdk->set_access_token($access_token_in_db);

$order = $sdk->get_order($order_id);
var_dump($order);

// Detect if submit button is pressed
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['createFulfillment'])){

    //
    // Based on order data from EasyStore API
    // you can setup an interface for seller fulfil their order through your service.
    //
    $order = $sdk->get_order($order_id);
    $order = $order["order"];

    //
    // Once seller completed their fulfil action
    // you need to create an EasyStore fulfillment via EasyStore API
    // so that the order's fulfillment status will become fulfilled
    //

    $fulfillment = $sdk->create_fulfillment($order_id, [

        "tracking_company" 		=> "PosLaju",                            // courier name
        "tracking_urls" 		=> "https://your-app-tracking-urls.com", // URL for buyer / seller to check the parcel status 
        "tracking_numbers" 		=> "EP00011323187924MY",                 // tracking ID
        "grams"					=> 1000,                                 // total fulfilled items weight in gram
        "status" 				=> "open",                               // current status for this fulfillment. 
                                                                        // (available values: 'open', 'delivered', 'in_transit', 'out_of_delivery', 'available_for_pickup', 'cancelled', 'failure')
        "is_mail"               => 1,                                    // indicator to send email notification to buyer. send = 1, not to send = 0.
        "message" 				=> "Download your airway bill <a href='https://airwaybills.com'>here</a>", // a short message to display in this order.
        "fulfillment_items"		=> [
            [
                'order_items_id' => $order["line_items"][0]["id"],
                'quantity'       => $order["line_items"][0]["quantity"],
            ]
        ], // which line items been fulfilled
    ]);

    $redirect_url = "https://admin.easystore.blue/orders/".$order_id;

    echo "<p>Fulfillment created successfully</p>";
    echo "<a href=".$redirect_url.">Back</a>";

}

$url = "fulfillment-url.php?shop=testestest.storefront.blue&order_id=".$order_id."&token=".$order_token;


?>



<html>

<head>
    <title>Sample Logistics App</title>
</head>

<body>

    <h3>Sample Logistics App Create Fulfillment</h3>
    <br>
    <p>Notes:</p>
    <ol>
        <li>Prepare interface for sellers to fulfil their order with your service.</li>
        <li>Input fields for information needed should be included along with validations.</li>
    </ol>

    <form action=<?php echo $url; ?> method="post">
        <input type="submit" name="createFulfillment" value="Create Fulfillment">
    </form>

    <button onclick="goBack()">Back to order</button>


<body>

</html>

<script>
    function goBack() {
        window.history.back();
    }
</script>