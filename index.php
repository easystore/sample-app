<?php 

// 
// This file serve as the main entrance. EasyStore user click on install this app will redirect to this URL.
// If you embedded your app in EasyStore. EasyStore will iframe this URL as well.
//

include '_inc.php';
include 'EasyStoreSDK.php';

$query = $_GET;
$query_hmac = $query["hmac"];
unset($query["hmac"]);

$sdk = new EasyStore(CLIENT_ID, CLIENT_SECRET, $_GET["shop"]);

$generated_hmac = $sdk->generate_hmac( urldecode(http_build_query($query)) );

if($sdk->verify_hmac($query_hmac, $generated_hmac)){

    //
    // use $query["shop"] to identify user in your database
    // E.g: select * from apps.easystore where shop = $query["shop"];
    //

    $exist_in_db = true;

    if( !$exist_in_db ){

        //redirect sellers to EasyStore platform to complete oauth process
        header("Location: https://admin.easystore.blue/oauth/authorize?app_id=".CLIENT_ID."&scope=".SCOPES."&redirect_uri=".REDIRECT_URL);
        
    }else{

        //show your app setting
        header("Location: setting.php");

    }


}else{

    throw new Exception("hmac invalid.");

}
